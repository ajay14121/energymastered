angular.module('aura-genie.controllers').controller('otpCtrl', function($scope, $http, $location, $ionicPopup, $rootScope, $ionicLoading,$localStorage,$state) {
    $scope.data = {};
    $scope.data.email = '';
    $scope.data.otp = '';
    $scope.confirmOTP = function() {
        if ($scope.data.email === '') {
            $ionicPopup.alert({
                title: 'empty field',
                template: 'email is mandatory'
            });
        } else if (!$rootScope.email_filter.test($scope.data.email)) {
            $ionicPopup.alert({
                title: 'invalid email',
                template: 'your email id is not valid'
            });
        } else if ($scope.data.otp == '') {
            $ionicPopup.alert({
                title: 'empty field',
                template: 'OTP is mandatory'
            });
        } else {
            $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/User/confirm_otp",
                method: 'POST',
                data: {
                    email: $scope.data.email,
                    otp: $scope.data.otp,
                    uuid: $rootScope.uuid,
                    platform: $rootScope.platform
                }
            }).success(function(response) {
                $ionicLoading.hide();
                $localStorage.user = response.data;
                $rootScope.user=$localStorage.user;
                if (response.status) {
                    $ionicPopup.alert({
                        title: "Email Confirmed Successfully",
                        template: response.message
                    }).then(function(res) {
                        console.log(response.status);
                   if (response.data.status == 1) {
                          $state.go("app.account_activation",null,{reload:true});
                    } else {
                        $state.go("app.user_record_one",null,{reload:true});
                    }
                    });
                } else {
                    $ionicPopup.alert({
                        title: "ERROR",
                        template: response.message
                    });
                }
            }).error(function(erorr) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'OTP conifermation failed',
                    template: 'API is not working'
                });
            });
        }
    };
});
    