angular.module('aura-genie.controllers').controller('userRecordTwoCtrl', function($scope, $http, $location, $ionicPopup, $rootScope, $ionicLoading, $stateParams, $localStorage,$state) {
    $scope.data = {};
    $scope.data.symptoms_detail = '';
    $scope.symptoms = [
        {id: 1, symptom_name: 'Allergies', selected: 'true'},
        {id: 2, symptom_name: 'Anxiety', selected: 'true'},
        {id: 3, symptom_name: 'Arthritis', selected: 'false'},
        {id: 4, symptom_name: 'Asthama', selected: 'false'},
        {id: 5, symptom_name: 'Bad Circulation', selected: 'false'},
        {id: 6, symptom_name: 'Bruise Easily', selected: 'false'},
        {id: 7, symptom_name: 'CardioVascular', selected: 'false'},
        {id: 8, symptom_name: 'Digertive Disease', selected: 'false'},
        {id: 9, symptom_name: 'Depression', selected: 'true'},
        {id: 10, symptom_name: 'Diabetes', selected: 'true'},
        {id: 11, symptom_name: 'Head Aches', selected: 'false'},
        {id: 12, symptom_name: 'High Blood Pressure', selected: 'false'},
        {id: 13, symptom_name: 'Painful Joints', selected: 'false'}
    ];
    $scope.suffer_from = {
        selections: []
    };

    $scope.beginVoiceAnalysis = function() {
          if($scope.data.symptoms_detail=='')
          {
              $ionicPopup.alert({
                    title: "Empty Field",
                    template: "please give description of symptom"
                });
          } else
          {
        $ionicLoading.show();
        $http({
            url: $rootScope.apiUrl + "API/User/update_user_record",
            method: 'POST',
            data: {
                first_name: $stateParams.user_first_name,
                last_name: $stateParams.user_last_name,
                dob: $stateParams.user_dob,
                suffer_from: $scope.suffer_from.selections,
                symptoms_detail: $scope.data.symptoms_detail,
                record_id: $stateParams.user_record_id
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            $ionicLoading.hide();
            if (response.status) {
                $ionicPopup.alert({
                    title: "Success",
                    template: response.message
                }).then(function(res) {
                    //$location.path("/app/voice_analysis");
                    $state.go("app.voice_analysis", {user_record_id: $stateParams.user_record_id});
                });
            } else {
                $ionicPopup.alert({
                    title: "authentication failed",
                    template: response.message
                });
            }
        }).error(function(erorr) {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'failed',
                template: 'API is not working'
            });
        });
    };
    };
});
    