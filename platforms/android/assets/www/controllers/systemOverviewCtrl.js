angular.module('aura-genie.controllers').controller('systemOverviewCtrl', function ($scope, $rootScope, $state, $timeout, $interval,$ionicPopup) {
    $scope.IsVisible = false;
    $scope.hideScan = false;
    $scope.classFooter = 'bar-subfooter';
    $scope.changeIconBRx = true;
    $scope.itemCountBRx=0;
    $scope.itemCountMRx=0;
    $scope.flag=false;
    $scope.playItemsBRx = [];
    $scope.playItemsMRx = [];
    $scope.IsVisibleMRx = false;
    $scope.findIndex = function (index)
    {
        $rootScope.index=index;
        $rootScope.paramList = $rootScope.system_overview[index].uri;
        if($scope.hideScan==false)
        {
            $ionicPopup.alert({
                title: 'Invalid Action',
                template: 'Please Click Scan'
            });
        }
        else {
            $state.go("app.system_overview_two");
        }
//        console.log($rootScope.paramList);
//        console.log($rootScope.title);
        
    };
    $scope.showPlayerBRx = function ()
    {
        $scope.IsVisibleBRx = true;
        $scope.changeIconBRx = false;
    };

    $scope.hidePlayerBRx = function ()
    {
        $scope.IsVisibleBRx = false;
        $scope.changeIconBRx = true;
    };
    $scope.onDropCompleteBRx = function (data, evt) {
           
            $scope.matches = true;
            angular.forEach($scope.playItemsBRx, function(item) {
                if (data.id === item.id) {
                    $scope.matches = false;
                }
            });

            // add item to collection
            if ($scope.matches != false) {
                 $scope.playItemsBRx.push(data);
            }
          // $scope.playItemsBRx = '';
           $scope.itemCountBRx=$scope.playItemsBRx.length;
       
    };
    $scope.showPlayerMRx = function ()
    {
        $scope.IsVisibleMRx = true;
        $scope.changeIconMRx = false;
    };
    $scope.hidePlayerMRx = function ()
    {
        $scope.IsVisibleMRx = false;
        $scope.changeIconMRx = true;
    };
    $scope.onDropCompleteMRx = function (data, evt) {
        $scope.matches = true;
            angular.forEach($scope.playItemsMRx, function(item) {
                if (data.id === item.id) {
                    $scope.matches = false;
                }
            });

            // add item to collection
            if ($scope.matches != false) {
                 $scope.playItemsMRx.push(data);
            }
        
        $scope.itemCountMRx=$scope.playItemsMRx.length;
    };
    $scope.getRandomValue = function () {
        $scope.hideScan = true;
        $scope.classFooter = 'bar-footer';
        $scope.intervalRef = $interval(function () {
            for (var i = 0; i < 5; i++) {

                $rootScope.randomValue[i] = parseInt(Math.random() * 999);
            }

        }, 100);
        var myTimer = $timeout(function () {
            $interval.cancel($scope.intervalRef);
            for (var index = 0; index < $rootScope.system_overview.mind.length; index++) {
                $rootScope.system_overview.mind[index].randomValue = parseInt(Math.random() * 999);
            }
            
            for (var index = 0; index < $rootScope.system_overview.body.length; index++) {
                $rootScope.system_overview.body[index].randomValue = parseInt(Math.random() * 999);
            }
            
            for (var index = 0; index < $rootScope.system_overview.energy.length; index++) {
                $rootScope.system_overview.energy[index].randomValue = parseInt(Math.random() * 999);
            }
            
            for (var index = 0; index < $rootScope.system_overview.etherics.length; index++) {
                $rootScope.system_overview.etherics[index].randomValue = parseInt(Math.random() * 999);
            }
            
            for (var index = 0; index < $rootScope.system_overview.libraries.length; index++) {
                $rootScope.system_overview.libraries[index].randomValue = parseInt(Math.random() * 999);
            }
            
            for (var index = 0; index < $rootScope.system_overview.mind.neurotransmitters.length; index++) {
                $rootScope.system_overview.mind.neurotransmitters[index].randomValue = parseInt(Math.random() * 999);
            }
            
            for (var index = 0; index < $rootScope.system_overview.mind.emotional.length; index++) {
                $rootScope.system_overview.mind.emotional[index].randomValue = parseInt(Math.random() * 999);
            }
            
            for (var index = 0; index < $rootScope.system_overview.mind.batchFlowerEssences.length; index++) {
                $rootScope.system_overview.mind.batchFlowerEssences[index].randomValue = parseInt(Math.random() * 999);
            }
            
            for (var index = 0; index < $rootScope.system_overview.mind.brainAnatomy.length; index++) {
                $rootScope.system_overview.mind.brainAnatomy[index].randomValue = parseInt(Math.random() * 999);
            }
            for (var index = 0; index < $rootScope.system_overview.energy.chakras.length; index++) {
                $rootScope.system_overview.energy.chakras[index].randomValue = parseInt(Math.random() * 999);
            }
            for (var index = 0; index < $rootScope.system_overview.body.vitamins.length; index++) {
                $rootScope.system_overview.body.vitamins[index].randomValue = parseInt(Math.random() * 999);
            }
            for (var index = 0; index < $rootScope.system_overview.body.organs.length; index++) {
                $rootScope.system_overview.body.organs[index].randomValue = parseInt(Math.random() * 999);
            }
            for (var index = 0; index < $rootScope.system_overview.body.currentInfections.length; index++) {
                $rootScope.system_overview.body.currentInfections[index].randomValue = parseInt(Math.random() * 999);
            }
            for (var index = 0; index < $rootScope.system_overview.body.digestion.length; index++) {
                $rootScope.system_overview.body.digestion[index].randomValue = parseInt(Math.random() * 999);
            }
            for (var index = 0; index < $rootScope.system_overview.body.spinalEnergy.length; index++) {
                $rootScope.system_overview.body.spinalEnergy[index].randomValue = parseInt(Math.random() * 999);
            }
            for (var index = 0; index < $rootScope.system_overview.body.bodySystems.length; index++) {
                $rootScope.system_overview.body.spinalEnergy[index].randomValue = parseInt(Math.random() * 999);
            }
        }, 1000);


        $scope.IsVisible = $scope.IsVisible ? false : true;

    };
//     $scope.descriptionMind=function() {
//          for(var i=0;i<6;i++){
//            $rootScope.randomMindValue[i]=parseInt(Math.random()*999);
//        }
//       $state.go("app.system_overview_three");
//     };
});