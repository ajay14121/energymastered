angular.module('aura-genie.controllers').controller('systemOverviewBodyCtrl', function($scope, $interval, $rootScope, $timeout) {
    angular.element(document).ready(function () {
           
        $scope.topVal=[0,0,0];
        $scope.maxIndex=[0,0,0];
        $scope.playItemsBRx=[];
        $scope.playItemsMRx=[];
        $scope.bodyParamList=[
            {
                id:0,title:"Organs",
            },
            {
                id:1,title:"Vitamins"
            },
            {
                id:2,title:"Essential Oils"
            },
            {
                id:3,title:"Herbs"
            },
            {
                id:4,title:"Minerals"
            },
            {
                id:5,title:"Amino Acids"
            },
            {
                id:6,title:"Glands"
            },
            {
                id:7,title:"Meridians"
            },
            {
                id:8,title:"Today's stress"
            },
            {
                id:9,title:"Food Senstivities"
            },
            {
                id:10,title:"Chemical Senstivities"
            },
            {
                id:11,title:"Eletrical Senstivities"
            },
            {
                id:12,title:"Body Systems"
            },
            {
                id:13,title:"Digestion"
            },
            {
                id:14,title:"Spinal Energy"
            },
            {
                id:15,title:"Current infections"
            },
            {
                id:16,title:"Hormones"
            }
        ]; 
        $scope.IsVisible = false;
        $scope.changeIcon = true;
        $scope.IsVisibleMRx = false;
        $scope.intervalRef = $interval(function () {
            for (var i = 0; i < $scope.bodyParamList.length; i++) {
                $rootScope.randomBodyValue[i] = parseInt(Math.random() * 999);
            }
            console.log("loop count="+i);
        }, 100);
        var myTimer = $timeout(function () {
            var canceled = $interval.cancel($scope.intervalRef);
            console.log(canceled);
            for (var i = 0; i < $rootScope.randomBodyValue.length; i++) {
                if ($rootScope.randomBodyValue[i] > $scope.topVal[0]) {
                    $scope.topVal[0] = $rootScope.randomBodyValue[i];
                    $scope.maxIndex[0] = i;
                    
                }
            }
             
            for (i = 0; i < $rootScope.randomBodyValue.length; i++) {
                if ($rootScope.randomBodyValue[i] > $scope.topVal[1] && $rootScope.randomBodyValue[i] < $scope.topVal[0]) {
                    $scope.topVal[1] = $rootScope.randomBodyValue[i];
                    $scope.maxIndex[1] = i;
                }
            }

            for (i = 0; i < $rootScope.randomBodyValue.length; i++) {
                if ($rootScope.randomBodyValue[i] > $scope.topVal[2] && $rootScope.randomBodyValue[i] < $scope.topVal[1]) {
                    $scope.topVal[2] = $rootScope.randomBodyValue[i];
                    $scope.maxIndex[2] = i;
                }
            }
            console.log($scope.maxIndex);
            console.log("bodyParam"+$scope.maxIndex[0]);
            document.getElementById("bodyParam" + $scope.maxIndex[0]).style.background = "pink";
            document.getElementById("bodyParam" + $scope.maxIndex[1]).style.background = "pink";
            document.getElementById("bodyParam" + $scope.maxIndex[2]).style.background = "pink";
            
            console.log($scope.topVal);
            
        },1000);

        $scope.showPlayerBRx = function ()
        {
            $scope.IsVisibleBRx = true;
            $scope.changeIconBRx = false;
        };

        $scope.hidePlayerBRx = function ()
        {
            $scope.IsVisibleBRx = false;
            $scope.changeIconBRx = true;
        };
        $scope.onDropCompleteBRx = function (data, evt) {
            $scope.playItemsBRx.push(data);
        };
        $scope.showPlayerMRx = function ()
        {
            $scope.IsVisibleMRx = true;
            $scope.changeIconMRx = false;
        };
        $scope.hidePlayerMRx = function ()
        {
            $scope.IsVisibleMRx = false;
            $scope.changeIconMRx = true;
        };
    $scope.onDropCompleteMRx=function(data,evt){
        $scope.playItemsMRx.push(data);
    };
   });
});
    