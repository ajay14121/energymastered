angular.module('aura-genie.controllers').controller('signupCtrl', function($scope, $http, $location, $ionicPopup, $rootScope, $ionicLoading) {
    $scope.data = {};
    $scope.data.first_name = '';
    $scope.data.last_name = '';
    $scope.data.email = '';
    $scope.data.password = '';

    $scope.signup = function() {
        console.log($scope.data);
        if ($scope.data.first_name == '') {
            $ionicPopup.alert({
                title: 'empty field',
                template: 'first name is mandatory'
            });
        } else if ($scope.data.last_name === '') {
            $ionicPopup.alert({
                title: 'empty field',
                template: 'last name is mandatory'
            });
        } else if ($scope.data.email === '') {
            $ionicPopup.alert({
                title: 'empty field',
                template: 'email is mandatory'
            });
        } else if (!$rootScope.email_filter.test($scope.data.email)) {
            $ionicPopup.alert({
                title: 'invalid email',
                template: 'your email id is not valid'
            });
        } else if ($scope.data.password === '') {
            $ionicPopup.alert({
                title: 'empty field',
                template: 'password is mandatory'
            });
        } else {
            $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/User/register",
                method: 'POST',
                data: {
                    email: $scope.data.email,
                    password: $scope.data.password,
                    first_name: $scope.data.first_name,
                    last_name: $scope.data.last_name,
                }
            }).success(function(response) {
                $ionicLoading.hide();
                if (response.status) {
                    $ionicPopup.alert({
                        title: "registration request sent",
                        template: response.data.email + "please check your email for verfication code"
                    }).then(function(res) {
                        console.log(res);
                        $location.path("/otp");
                    });
                } else {
                    $ionicPopup.alert({
                        title: "registration failed",
                        template: response.message
                    });
                }
            }).error(function(erorr) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'signup failed',
                    template: 'API is not working'
                });
            });
        }
    };
});
    