angular.module('aura-genie.controllers').controller('menuCtrl', function($scope, $state,$localStorage,$cordovaFacebook,$rootScope) {
    $rootScope.user = $localStorage.user;
    $scope.logout = function() {
        $localStorage.$reset();
        $state.go("login");
        console.log("controller working");
   
    
    try{
        $cordovaFacebook.logout()
        .then(function(success) {
          // success
          console.log('Facebook Logout Success');
        }, function (error) {
          // error
          console.log('Facebook Logout Fail');
        });
    } catch(e){
        console.log('Facebook Logout only work on Device');
        console.log(e);
    }
 };
});
    