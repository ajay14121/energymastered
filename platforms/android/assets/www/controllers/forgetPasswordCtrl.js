angular.module('aura-genie.controllers').controller('forgetPasswordCtrl', function($scope, $http, $location, $ionicPopup, $rootScope, $ionicLoading) {
    $scope.data = {};
    $scope.data.email = '';
    console.log($scope.data);
    $scope.forgetPassword = function() {
        if ($scope.data.email === '') {
            $ionicPopup.alert({
                title: 'empty field',
                template: 'Please enter email id'
            });
        } else if (!$rootScope.email_filter.test($scope.data.email)) {
            $ionicPopup.alert({
                title: 'invalid email',
                template: 'Please enter valid email'
            });
        } else {
            $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/User/forgot_password",
                method: 'POST',
                data: {
                    email: $scope.data.email,
                }
            }).success(function(response) {
                $ionicLoading.hide();
                if (response.status) {
                    $ionicPopup.alert({
                        title: "Success",
                        template: response.message
                    }).then(function(res) {
                        $location.path("/login");
                    });
                } else {
                    $ionicPopup.alert({
                        title: "Fail",
                        template: response.message
                    });
                }
            }).error(function(erorr) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Process failed',
                    template: 'API is not working'
                });
            });
        }
    };
});
    