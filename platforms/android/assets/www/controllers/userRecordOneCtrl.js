angular.module('aura-genie.controllers').controller('userRecordOneCtrl', function($scope, $http, $location, $ionicPopup, $rootScope, $ionicLoading, $localStorage, $state, $stateParams) {
    $scope.data = {};
    $scope.data.first_name = '';
    $scope.data.last_name = '';
    $scope.data.dob = '';

    // Date of Birth Call Back functions
    $scope.datePickerCallback = function(val) {
        if (typeof (val) === 'undefined') {
            console.log('Date not selected.');
        } else {
            $scope.data.dob = val.getFullYear() + '-' + (val.getMonth() + 1) + '-' + val.getDate();
            console.log('Selected date is : ', val);
        }
    };

    $scope.sendDetailOne = function() {
        if ($scope.data.first_name == '') {
            $ionicPopup.alert({
                title: 'empty field',
                template: 'First Name is mandatory'
            });
        } else if ($scope.data.last_name == '') {
            $ionicPopup.alert({
                title: 'empty field',
                template: 'Last Name is mandatory'
            });
        } else if ($scope.data.dob == '') {
            $ionicPopup.alert({
                title: 'empty field',
                template: 'Date of Birth is mandatory'
            });
        } else {
            $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/User/add_user_record",
                method: 'POST',
                data: {
                    first_name: $scope.data.first_name,
                    last_name: $scope.data.last_name,
                    dob: $scope.data.dob
                },
                headers: {
                    'Access-Token': $localStorage.user.token,
                    'Email': $localStorage.user.email
                }
            }).success(function(response) {
                $ionicLoading.hide();
                if (response.status) {
                    $ionicPopup.alert({
                        title: "Success",
                        template: response.message
                    }).then(function(alert_response) {
                        $state.go("app.user_record_two", {user_record_id: response.data.record_id});
                    });
                } else {
                    $ionicPopup.alert({
                        title: "failed",
                        template: response.message
                    });
                }
            }).error(function(erorr) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'API error',
                    template: 'API is not working'
                });
            });
        }
    };
});
    