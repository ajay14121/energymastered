angular.module('aura-genie.controllers').controller('emotionalCtrl', function($scope, $interval, $rootScope, $timeout, $cordovaMedia, $cordovaCapture) {
    angular.element(document).ready(function () {
           
        $scope.topVal=[0,0,0];
        $scope.maxIndex=[0,0,0];
        $scope.playItemsBRx=[];
        $scope.playItemsMRx=[];
        $scope.emotionalParamList=[
            {
                id:0,title:"Affection"
            },
            {
                id:1,title:"Anger"
            },
            {
                id:2,title:"Angst"
            },
            {
                id:3,title:"Annoyance"
            },
            {
                id:4,title:"Anxiety"
            },
            {
                id:5,title:"Apathy"
            },
            {
                id:6,title:"Awe"
            },
            {
                id:7,title:"Borebom"
            },
            {
                id:8,title:"Confusion"
            },
            {
                id:9,title:"Contempt"
            },
            {
                id:10,title:"Curiosity"
            },
            {
                id:11,title:"Depression"
            },
            {
                id:12,title:"Desire"
            },
            {
                id:13,title:"Despair"
            },
            {
                id:14,title:"Disappointment"
            },
            {
                id:15,title:"Disgust"
            },
            {
                id:16,title:"Ecstasy"
            },
            {
                id:17,title:"Embarrassement"
            },
            {
                id:18,title:"Enpathy"
            },
            {
                id:19,title:"Envy"
            },
            {
                id:20,title:"Euphoria"
            },
            {
                id:21,title:"Fear"
            },
            {
                id:22,title:"Fear"
            },
            {
                id:23,title:"Frustration"
            },
            {
                id:24,title:"Gratitude"
            },
            {
                id:25,title:"Grief"
            },
            {
                id:26,title:"Guilt"
            },
            {
                id:27,title:"Happiness"
            },
            {
                id:28,title:"Hatred"
            },
            {
                id:29,title:"Hope"
            },
            {
                id:30,title:"Horror"
            },
            {
                id:31,title:"Hostility"
            },
            {
                id:32,title:"Hysteric"
            },
            {
                id:33,title:"Interest"
            },
            {
                id:34,title:"Jealousy"
            },
            {
                id:35,title:"Loathing"
            },
            {
                id:36,title:"Loneliness"
            },
            {
                id:37,title:"Love"
            },
            {
                id:38,title:"Lust"
            }
            ,
            {
                id:39,title:"Misery"
            },
            {
                id:40,title:"Pity"
            },
            {
                id:41,title:"Pride"
            },
            {
                id:42,title:"Rage"
            }
            ,
            {
                id:43,title:"Regret"
            },
            {
                id:44,title:"Remorse"
            },
            {
                id:45,title:"Sad"
            }
        ]; 
        $scope.IsVisible = false;
        $scope.changeIcon = true;
        $scope.IsVisibleMRx = false;
        $scope.intervalRef = $interval(function () {
            for (var i = 0; i < $scope.emotionalParamList.length; i++) {
                $rootScope.randomEmotionalValue[i] = parseInt(Math.random() * 999);
            }
        }, 100);
        var myTimer = $timeout(function () {
            $interval.cancel($scope.intervalRef);
            for (var i = 0; i < $rootScope.randomEmotionalValue.length; i++) {
                if ($rootScope.randomEmotionalValue[i] > $scope.topVal[0]) {
                    $scope.topVal[0] = $rootScope.randomEmotionalValue[i];
                    $scope.maxIndex[0] = i;
                }
            }

            for (i = 0; i < $rootScope.randomEmotionalValue.length; i++) {
                if ($rootScope.randomEmotionalValue[i] > $scope.topVal[1] && $rootScope.randomEmotionalValue[i] < $scope.topVal[0]) {
                    $scope.topVal[1] = $rootScope.randomEmotionalValue[i];
                    $scope.maxIndex[1] = i;
                }
            }

            for (i = 0; i < $rootScope.randomEmotionalValue.length; i++) {
                if ($rootScope.randomEmotionalValue[i] > $scope.topVal[2] && $rootScope.randomEmotionalValue[i] < $scope.topVal[1]) {
                    $scope.topVal[2] = $rootScope.randomEmotionalValue[i];
                    $scope.maxIndex[2] = i;
                }
            }
            document.getElementById("emotionalParam" + $scope.maxIndex[0]).style.background = "pink";
            document.getElementById("emotionalParam" + $scope.maxIndex[1]).style.background = "pink";
            document.getElementById("emotionalParam" + $scope.maxIndex[2]).style.background = "pink";
        }, 1000);

        $scope.showPlayerBRx = function ()
        {
            $scope.IsVisibleBRx = true;
            $scope.changeIconBRx = false;
        };

        $scope.hidePlayerBRx = function ()
        {
            $scope.IsVisibleBRx = false;
            $scope.changeIconBRx = true;
        };
        $scope.onDropCompleteBRx = function (data, evt) {
            $scope.playItemsBRx.push(data);
        };
        $scope.showPlayerMRx = function ()
        {
            $scope.IsVisibleMRx = true;
            $scope.changeIconMRx = false;
        };
        $scope.hidePlayerMRx = function ()
        {
            $scope.IsVisibleMRx = false;
            $scope.changeIconMRx = true;
        };
    $scope.onDropCompleteMRx=function(data,evt){
        $scope.playItemsMRx.push(data);
    };
   });
});
    