// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('aura-genie', ['ionic', 'aura-genie.controllers', 'ionic-datepicker', 'ngCordova', 'ngStorage', 'ngDraggable'])
        .run(function ($ionicPlatform, $rootScope, $localStorage, $cordovaDevice) {
            $ionicPlatform.ready(function () {
                //$rootScope.apiUrl = "http://10.10.10.82:81/projects/aura_genie/";
                $rootScope.apiUrl = "http://giftsoninternet.com/all/aura_genie/";
                $rootScope.randomValue = [];
                $rootScope.paramList = '';
                $rootScope.randomMindValue = [];
                $rootScope.randomBodyValue = [];
                $rootScope.randomEnergyValue = [];

                $rootScope.system_overview = [
                    {
                        id: 0, title: "Mind", uri: "mind",
                        mind: [
                        ]
                    },
                    {
                        id: 1,
                        title: "Body",
                        uri: "body",
                        body: [
                        ]

                    },
                    {
                        id: 2,
                        title: "Energy",
                        uri: "energy",
                        energy: [
                        ]
                    },
                    {
                        id: 3,
                        title: "Etherics",
                        uri: "etherics",
                        etherics: [
                        ]
                    },
                    {
                        id: 4,
                        title: "Libraries",
                        uri: "libraries",
                        libraries: [
                        ]
                    }
                ];

                $rootScope.system_overview.mind = [
                    {
                        id: 0, title: "Emotional", uri: "emotional", topClassActive: "", randomValue: '',
                        emotional: [
                        ]
                    },
                    {
                        id: 1, title: "Brain EEG", uri: "brainEeg", topClassActive: "", randomValue: '',
                        brainEeg: [
                        ]
                    },
                    {
                        id: 2, title: "Batch Flower Essences", uri: "batchFlowerEssences", topClassActive: "", randomValue: '',
                        batchFlowerEssences: [
                        ]

                    },
                    {
                        id: 3, title: "Neurotransmitters", uri: "neurotransmitters", topClassActive: "", randomValue: '',
                        neurotransmitters: [
                        ]
                    },
                    {
                        id: 4, title: "Brain Anatomy", uri: "brainAnatomy", topClassActive: "", randomValue: '',
                        brainAnatomy: [
                        ]
                    },
                    {
                        id: 5, title: "Past Tramaus Affecting", uri: "pastTramaus", topClassActive: "", randomValue: '',
                        pastTraumas: [
                        ]
                    }
                ];
                $rootScope.system_overview.mind.batchFlowerEssences = [
                    {
                        id: 0, title: "Agrimony", randomValue: '',
                    },
                    {
                        id: 1, title: "Aspen", randomValue: '',
                    },
                    {
                        id: 2, title: "Beech", randomValue: '',
                    },
                    {
                        id: 3, title: "Centaury", randomValue: '',
                    },
                    {
                        id: 4, title: "Cerato", randomValue: '',
                    },
                    {
                        id: 5, title: "Chicory", randomValue: '',
                    },
                    {
                        id: 6, title: "Clematis", randomValue: '',
                    },
                    {
                        id: 7, title: "Crab Apple", randomValue: '',
                    },
                    {
                        id: 8, title: "Elm", randomValue: '',
                    },
                    {
                        id: 9, title: "Gentian", randomValue: '',
                    },
                    {
                        id: 10, title: "Gorse", randomValue: '',
                    },
                    {
                        id: 11, title: "Heather", randomValue: '',
                    },
                    {
                        id: 12, title: "ch flowers", randomValue: '',
                    },
                    {
                        id: 13, title: "Holly", randomValue: '',
                    },
                    {
                        id: 14, title: "Hunnysuckle", randomValue: '',
                    },
                    {
                        id: 16, title: "Homebeam", randomValue: '',
                    },
                    {
                        id: 17, title: "Impatiens", randomValue: '',
                    },
                    {
                        id: 18, title: "Larch", randomValue: '',
                    },
                    {
                        id: 19, title: "Mimulus", randomValue: '',
                    },
                    {
                        id: 20, title: "Mustard", randomValue: '',
                    },
                    {
                        id: 21, title: "Oak", randomValue: '',
                    },
                    {
                        id: 22, title: "Olive", randomValue: '',
                    },
                    {
                        id: 23, title: "Pine", randomValue: '',
                    },
                    {
                        id: 24, title: "Red Chestnut", randomValue: '',
                    },
                    {
                        id: 25, title: "Rock Rose", randomValue: '',
                    },
                    {
                        id: 26, title: "Rock Water", randomValue: '',
                    },
                    {
                        id: 27, title: "Scleranthus", randomValue: '',
                    },
                    {
                        id: 28, title: "Star of Bethlehem", randomValue: '',
                    },
                    {
                        id: 29, title: "Sweet Chestnut", randomValue: '',
                    },
                    {
                        id: 30, title: "Vervian", randomValue: '',
                    },
                    {
                        id: 31, title: "Vine", randomValue: '',
                    },
                    {
                        id: 32, title: "Walnet", randomValue: '',
                    },
                    {
                        id: 33, title: "Water Violet", randomValue: '',
                    },
                    {
                        id: 34, title: "White Chestnut", randomValue: '',
                    },
                    {
                        id: 35, title: "Wild Oat", randomValue: '',
                    },
                    {
                        id: 36, title: "Wild Rose", randomValue: '',
                    },
                    {
                        id: 37, title: "Willlow", randomValue: '',
                    }
                ];
                $rootScope.system_overview.mind.brainAnatomy = [
                    {
                        id: 0, title: "Corex", randomValue: '',
                    },
                    {
                        id: 1, title: "Brain Stem", randomValue: '',
                    },
                    {
                        id: 2, title: "Basal Ganglia", randomValue: '',
                    },
                    {
                        id: 3, title: "Cerebellum", randomValue: '',
                    },
                    {
                        id: 4, title: "Frontal Lobes", randomValue: '',
                    },
                    {
                        id: 5, title: " Parietal Lobes", randomValue: '',
                    },
                    {
                        id: 6, title: "Temporal Lobes", randomValue: '',
                    },
                    {
                        id: 7, title: "Occipital Lobes", randomValue: '',
                    },
                    {
                        id: 8, title: "Cranium", randomValue: '',
                    },
                    {
                        id: 9, title: "Spinal Cord", randomValue: '',
                    },
                    {
                        id: 10, title: "Dura", randomValue: '',
                    }
                ];
                $rootScope.system_overview.mind.emotional = [
                    {
                        id: 0, title: "Affection", randomValue: '',
                    },
                    {
                        id: 1, title: "Anger", randomValue: '',
                    },
                    {
                        id: 2, title: "Angst", randomValue: '',
                    },
                    {
                        id: 3, title: "Annoyance", randomValue: '',
                    },
                    {
                        id: 4, title: "Anxiety", randomValue: '',
                    },
                    {
                        id: 5, title: "Apathy", randomValue: '',
                    },
                    {
                        id: 6, title: "Awe", randomValue: '',
                    },
                    {
                        id: 7, title: "Borebom", randomValue: '',
                    },
                    {
                        id: 8, title: "Confusion", randomValue: '',
                    },
                    {
                        id: 9, title: "Contempt", randomValue: '',
                    },
                    {
                        id: 10, title: "Curiosity", randomValue: '',
                    },
                    {
                        id: 11, title: "Depression", randomValue: '',
                    },
                    {
                        id: 12, title: "Desire", randomValue: '',
                    },
                    {
                        id: 13, title: "Despair", randomValue: '',
                    },
                    {
                        id: 14, title: "Disappointment", randomValue: '',
                    },
                    {
                        id: 15, title: "Disgust", randomValue: '',
                    },
                    {
                        id: 16, title: "Ecstasy", randomValue: '',
                    },
                    {
                        id: 17, title: "Embarrassement", randomValue: '',
                    },
                    {
                        id: 18, title: "Enpathy", randomValue: '',
                    },
                    {
                        id: 19, title: "Envy", randomValue: '',
                    },
                    {
                        id: 20, title: "Euphoria", randomValue: '',
                    },
                    {
                        id: 21, title: "Fear", randomValue: '',
                    },
                    {
                        id: 22, title: "Fear", randomValue: '',
                    },
                    {
                        id: 23, title: "Frustration", randomValue: '',
                    },
                    {
                        id: 24, title: "Gratitude", randomValue: '',
                    },
                    {
                        id: 25, title: "Grief", randomValue: '',
                    },
                    {
                        id: 26, title: "Guilt", randomValue: '',
                    },
                    {
                        id: 27, title: "Happiness", randomValue: '',
                    },
                    {
                        id: 28, title: "Hatred", randomValue: '',
                    },
                    {
                        id: 29, title: "Hope", randomValue: '',
                    },
                    {
                        id: 30, title: "Horror", randomValue: '',
                    },
                    {
                        id: 31, title: "Hostility", randomValue: '',
                    },
                    {
                        id: 32, title: "Hysteric", randomValue: '',
                    },
                    {
                        id: 33, title: "Interest", randomValue: '',
                    },
                    {
                        id: 34, title: "Jealousy", randomValue: '',
                    },
                    {
                        id: 35, title: "Loathing", randomValue: '',
                    },
                    {
                        id: 36, title: "Loneliness", randomValue: '',
                    },
                    {
                        id: 37, title: "Love", randomValue: '',
                    },
                    {
                        id: 38, title: "Lust", randomValue: '',
                    }
                    ,
                    {
                        id: 39, title: "Misery", randomValue: '',
                    },
                    {
                        id: 40, title: "Pity", randomValue: '',
                    },
                    {
                        id: 41, title: "Pride", randomValue: '',
                    },
                    {
                        id: 42, title: "Rage", randomValue: '',
                    }
                    ,
                    {
                        id: 43, title: "Regret", randomValue: '',
                    },
                    {
                        id: 44, title: "Remorse", randomValue: '',
                    },
                    {
                        id: 45, title: "Sad", randomValue: '',
                    }
                ];
                $rootScope.system_overview.mind.neurotransmitters = [
                    {
                        id: 0, title: "Serotonin", randomValue: '',
                    },
                    {
                        id: 1, title: "Gaba", randomValue: '',
                    },
                    {
                        id: 2, title: "Dopamine", randomValue: '',
                    },
                    {
                        id: 3, title: "Norepinephr", randomValue: '',
                    },
                    {
                        id: 4, title: "Epinephrine", randomValue: '',
                    }
                ];
                $rootScope.system_overview.body = [
                    {
                        id: 0, title: "Organs", uri: "organs", randomValue: '', organs: []
                    },
                    {
                        id: 1, title: "Vitamins", uri: "vitamins", randomValue: '', vitamins: []
                    },
                    {
                        id: 2, title: "Essential Oils", uri: "essentialOils", randomValue: '', essentialOils: []
                    },
                    {
                        id: 3, title: "Herbs", uri: "herbs", randomValue: '', herbs: []
                    },
                    {
                        id: 4, title: "Minerals", uri: "minerals", randomValue: '', minerals: []
                    },
                    {
                        id: 5, title: "Amino Acids", uri: "aminoAcids", randomValue: '', aminoAcids: []
                    },
                    {
                        id: 6, title: "Glands", uri: "glands", randomValue: '', glands: []
                    },
                    {
                        id: 7, title: "Meridians", uri: "merdians", randomValue: '', merdians: []
                    },
                    {
                        id: 8, title: "Today's stress", uri: "todayStress", randomValue: '', todayStress: []
                    },
                    {
                        id: 9, title: "Food Senstivities", uri: "foodSensitivies", randomValue: '', foodSensitivies: []
                    },
                    {
                        id: 10, title: "Chemical Senstivities", uri: "chemicalSenstivitiy", randomValue: '', chemicalSenstivitiy: []
                    },
                    {
                        id: 11, title: "Eletrical Senstivities", uri: "electricalSenstivity", randomValue: '', electricalSenstivity: []
                    },
                    {
                        id: 12, title: "Body Systems", uri: "bodySystems", randomValue: '', bodySystems: []
                    },
                    {
                        id: 13, title: "Digestion", uri: "digestion", randomValue: '', digestion: []
                    },
                    {
                        id: 14, title: "Spinal Energy", uri: "spinalEnergy", randomValue: '', spinalEnergy: []
                    },
                    {
                        id: 15, title: "Current infections", uri: "currentInfections", randomValue: '', currentInfections: []
                    },
                    {
                        id: 16, title: "Hormones", uri: "hormones", randomValue: '', hormones: []
                    }
                ];
                $rootScope.system_overview.body.organs = [
                    {
                        id: 0, title: "Brain"
                    },
                    {
                        id: 1, title: "Eyes"
                    },
                    {
                        id: 2, title: "Skin"
                    },
                    {
                        id: 3, title: "Thyroid"
                    },
                    {
                        id: 4, title: "Lungs"
                    },
                    {
                        id: 5, title: "Heart"
                    },
                    {
                        id: 6, title: "Liver"
                    },
                    {
                        id: 7, title: "Pancreas"
                    },
                    {
                        id: 8, title: "Stomach"
                    },
                    {
                        id: 9, title: "Spleen"
                    },
                    {
                        id: 10, title: "Kidneys"
                    },
                    {
                        id: 11, title: "Prostate"
                    },
                    {
                        id: 12, title: "Bladder"
                    }
                ];		
                $rootScope.system_overview.body.currentInfections=[
                     {
                        id: 0, title: "Virus"
                    },
                     {
                        id: 1, title: "Parasites"
                    },
                     {
                        id: 2, title: "Bacteria"
                    },
                     {
                        id: 3, title: "Biofilm"
                    },
                     {
                        id: 4, title: "Lyme"
                    }
                ];
                $rootScope.system_overview.body.vitamins=[
                    {
                        id: 0, title: "Vitamin A"
                    },
                     {
                        id: 1, title: "Vitamin B1"
                    },
                     {
                        id: 2, title: "Vitamin B2"
                    },
                     {
                        id: 3, title: "Vitamin B3"
                    },
                     {
                        id: 4, title: "Vitamin B4"
                    },
                     {
                        id: 5, title: "Vitamin B5"
                    },
                     {
                        id: 6, title: "Vitamin B6"
                    },
                     {
                        id: 7, title: "Vitamin B12"
                    },
                     {
                        id: 8, title: "Biotin"
                    },
                     {
                        id: 9, title: "Vitamin C"
                    },
                     {
                        id: 10, title: "Vitamin D"
                    },
                     {
                        id: 11, title: "Vitamin E"
                    },
                     {
                        id: 12, title: "Folic Acid"
                    },
                    {
                        id: 13, title: "Vitamin K"
                    }
                ];
                $rootScope.system_overview.body.spinalEnergy=[
                     {
                        id: 0, title: "C1"
                    },
                     {
                        id: 1, title: "C2"
                    },
                     {
                        id: 2, title: "C3"
                    },
                     {
                        id: 3, title: "C4"
                    },
                     {
                        id: 4, title: "C5"
                    },
                     {
                        id: 5, title: "C6"
                    },
                     {
                        id: 6, title: "C7"
                    },
                     {
                        id: 7, title: "Th1"
                    },
                     {
                        id: 8, title: "Th2"
                    },
                     {
                        id: 9, title: "Th3"
                    },
                     {
                        id: 10, title: "Th4"
                    },
                     {
                        id: 11, title: "Th5"
                    },
                     {
                        id: 12, title: "Th6"
                    },
                     {
                        id: 13, title: "Th7"
                    },
                     {
                        id: 14, title: "Th8"
                    },
                     {
                        id: 15, title: "Th9"
                    },
                     {
                        id: 16, title: "Th10"
                    },
                     {
                        id: 17, title: "Th11"
                    },
                     {
                        id: 18, title: "Th12"
                    },
                     {
                        id: 19, title: "L1"
                    },
                     {
                        id: 20, title: "L2"
                    },
                     {
                        id: 21, title: "L3"
                    },
                     {
                        id: 22, title: "L4"
                    },
                     {
                        id: 23, title: "L5"
                    },
                     {
                        id: 24, title: "Sacral"
                    }
                ];
                $rootScope.system_overview.body.bodySystems=[
                    {
                        id: 0, title: "Circulatory"
                    },
                     {
                        id: 1, title: "Digestive"
                    },
                     {
                        id: 2, title: "Endocrine"
                    },
                     {
                        id: 3, title: "Immune"
                    },
                     {
                        id: 4, title: "Integumentary"
                    },
                     {
                        id: 5, title: "Lymphatic"
                    },
                     {
                        id: 6, title: "Muscular"
                    },
                     {
                        id: 7, title: "Nervous"
                    },
                     {
                        id: 8, title: "Reproductive"
                    },
                     {
                        id: 9, title: "Respiratory"
                    },
                     {
                        id: 10, title: "Skeletal"
                    },
                     {
                        id: 11, title: "Urinary"
                    }
                ];
                $rootScope.system_overview.body.digestion=[
                     {
                        id: 0, title: "Liver"
                    },
                     {
                        id: 1, title: "Pancreas"
                    },
                     {
                        id: 2, title: "Gall Bladder"
                    },
                     {
                        id: 3, title: "Mouth"
                    },
                     {
                        id: 4, title: "Esophagus"
                    },
                     {
                        id: 5, title: "Stomach"
                    },
                     {
                        id: 6, title: "Small Intestine"
                    },
                     {
                        id: 7, title: "Large Intestine"
                    },
                     {
                        id: 8, title: "Flora"
                    },
                     {
                        id: 9, title: "Enzymes"
                    }
                ];
                $rootScope.system_overview.energy = [
                    {
                        id: 0, title: "Soifeggio Tones", uri: "soifeggioTones", randomValue: '', soifeggioTones: []
                    },
                    {
                        id: 1, title: "Sacred Geometry", uri: "sacredGeometry", randomValue: '', sacredGeometry: []
                    },
                    {
                        id: 2, title: "Chakras", uri: "chakras", randomValue: '', chakras: []
                    },
                    {
                        id: 3, title: "Auras", uri: "auras", randomValue: '', auras: []
                    },
                    {
                        id: 4, title: "nogier", uri: "nogier", randomValue: '', nogier: []
                    }
                ];
                $rootScope.system_overview.energy.soifeggioTones = [
                ];
                $rootScope.system_overview.energy.sacredGeometry = [
                ];
                $rootScope.system_overview.energy.chakras = [
                    {
                        id: 0, title: "Crown Chakra", randomValue: ''
                    },
                    {
                        id: 1, title: "Throat Chakra", randomValue: ''
                    },
                    {
                        id: 2, title: "Heart Chakra", randomValue: ''
                    },
                    {
                        id: 3, title: "Solar Chakra", randomValue: ''
                    },
                    {
                        id: 4, title: "Sacral Chakra", randomValue: ''
                    },
                    {
                        id: 5, title: "Root Chakra", randomValue: ''
                    }
                ];
                $rootScope.system_overview.energy.auras = [
                ];
                $rootScope.system_overview.energy.nogier = [
                    {
                        id: 0, title: "Nogier A", randomValue: ''
                    },
                    {
                        id: 1, title: "Nogier B", randomValue: ''
                    },
                    {
                        id: 2, title: "Nogier C", randomValue: ''
                    },
                    {
                        id: 3, title: "Nogier D", randomValue: ''
                    },
                    {
                        id: 4, title: "Nogier E", randomValue: ''
                    },
                    {
                        id: 5, title: "Nogier F", randomValue: ''
                    },
                    {
                        id: 6, title: "Nogier G", randomValue: ''
                    },
                    {
                        id: 7, title: "Nogier H", randomValue: ''
                    },
                    {
                        id: 8, title: "Nogier I", randomValue: ''
                    },
                ];
                $rootScope.system_overview.etherics = [
                    {
                        id: 0, title: "item1", randomValue: ''
                    },
                    {
                        id: 1, title: "item2", randomValue: ''
                    },
                    {
                        id: 2, title: "item3", randomValue: ''
                    },
                    {
                        id: 3, title: "item4", randomValue: ''
                    },
                    {
                        id: 4, title: "item5", randomValue: ''
                    }
                ];
                $rootScope.system_overview.libraries = [
                    {
                        id: 0, title: "item1", randomValue: ''
                    },
                    {
                        id: 1, title: "item2", randomValue: ''
                    },
                    {
                        id: 2, title: "item3", randomValue: ''
                    },
                    {
                        id: 3, title: "item4", randomValue: ''
                    },
                    {
                        id: 4, title: "item5", randomValue: ''
                    }
                ];

                // Set uuid & platform work only on device
                try {
                    $rootScope.uuid = $cordovaDevice.getUUID();
                    $rootScope.platform = $cordovaDevice.getPlatform();
                } catch (e) {
                    $rootScope.uuid = '';
                    $rootScope.platform = 'browser';
                }



                // Offline userLogin Count

                $rootScope.email_filter = /^[a-zA-Z0-9._]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                if (window.cordova && window.cordova.plugins.Keyboard) {
                    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                    // for form inputs)
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                    // Don't remove this line unless you know what you are doing. It stops the viewport
                    // from snapping when text inputs are focused. Ionic handles this internally for
                    // a much nicer keyboard experience.
                    cordova.plugins.Keyboard.disableScroll(true);
                }
                if (window.StatusBar) {
                    StatusBar.styleDefault();
                }

//                  // Login User Detail Store
//                if(!$localStorage.user){
//                    $localStorage.user = '';
//                }
            });
        })
        .config(function ($stateProvider, $urlRouterProvider, $httpProvider, $localStorageProvider) {
//            $httpProvider.defaults.headers.put = { 
//                'Accept' : 'application/json',
//                'Content-Type': 'application/json'
//            };

            $stateProvider
                    .state('app', {
                        url: '/app',
                        abstract: true,
                        templateUrl: 'templates/menu.html',
                        controller: 'menuCtrl'
                    })
                    .state('login', {
                        url: '/login',
                        templateUrl: 'templates/login.html',
                        controller: 'loginCtrl'

                    })
                    .state('app.voice_analysis', {
                        url: '/voice_analysis/:user_record_id',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/voice_analysis.html',
                                controller: 'voiceAnalysisCtrl'
                            }
                        }
                    })
                    .state('app.accessories', {
                        url: '/accessories',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/accessories.html',
                                controller: 'accessoriesCtrl'
                            }
                        }
                    })
                    .state('app.supportive', {
                        url: '/supportive',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/supportive.html',
                                controller: 'supportiveCtrl'
                            }
                        }
                    })
                    .state('app.setting', {
                        url: '/setting',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/setting.html',
                                controller: 'settingCtrl'
                            }
                        }
                    })
                    .state('app.frequency', {
                        url: '/frequency',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/frequency.html',
                                controller: 'frequencyCtrl'
                            }
                        }
                    })
                    .state('app.libraries', {
                        url: '/libraries',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/libraries.html',
                                controller: 'librariesCtrl'
                            }
                        }
                    })
                    .state('app.image_analysis', {
                        url: '/image_analysis/:user_record_id',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/image_analysis.html',
                                controller: 'imageAnalysisCtrl'
                            }
                        }
                    })

                    .state('app.system_overview', {
                        url: '/system_overview',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/system_overview.html',
                                controller: 'systemOverviewCtrl'
                            }
                        }
                    })

                    .state('app.system_overview_two', {
                        url: '/system_overview_two',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/system_overview_two.html',
                                controller: 'systemOverviewTwoCtrl'
                            }
                        }
                    })
                    .state('app.system_overview_three', {
                        url: '/system_overview_three',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/system_overview_three.html',
                                controller: 'systemOverviewThreeCtrl'
                            }
                        }
                    })
                    .state('app.past_record', {
                        url: '/past_record',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/past_record.html',
                                controller: 'pastRecordCtrl'
                            }
                        }
                    })
                    .state('forget_password', {
                        url: '/forget_password',
                        templateUrl: 'templates/forget_password.html',
                        controller: 'forgetPasswordCtrl'
                    })
                    .state('signup', {
                        url: '/signup',
                        templateUrl: 'templates/signup.html',
                        controller: 'signupCtrl'
                    })
                    .state('app.account_activation', {
                        url: '/account_activation',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/account_activation.html',
                                controller: 'accountActivationCtrl'
                            }
                        }
                    })
                    .state('app.user_records', {
                        url: '/user_records',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/user_records.html',
                                controller: 'userRecordsCtrl'
                            }
                        }
                    })
                    .state('otp', {
                        url: '/otp',
                        templateUrl: 'templates/otp.html',
                        controller: 'otpCtrl'
                    })
                    .state('app.user_record_one', {
                        url: '/user_record_one',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/user_record_one.html',
                                controller: 'userRecordOneCtrl'
                            }
                        }
                    })
                    .state('app.user_record_two', {
                        url: '/user_record_two/:user_record_id',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/user_record_two.html',
                                controller: 'userRecordTwoCtrl'
                            }
                        }
                    });

            // If user is logged in then redirect on user record page        
            if ($localStorageProvider.$get().user == undefined) {
                $urlRouterProvider.otherwise("/login");
            } else {
                $urlRouterProvider.otherwise("/app/user_record_one");
            }
        });
