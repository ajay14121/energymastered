angular.module('aura-genie.controllers').controller('systemOverviewMindCtrl', function($scope, $interval, $rootScope, $timeout, $cordovaMedia, $cordovaCapture) {
    angular.element(document).ready(function () {
           
        $scope.topVal=[0,0,0];
        $scope.maxIndex=[0,0,0];
        $scope.playItemsBRx=[];
        $scope.playItemsMRx=[];
        $scope.mindParamList=[
            {
                id:0,title:"Emotional",uri:"emotional"
            },
            {
                id:1,title:"Brain EEG",uri:"brain_eeg"
            },
            {
                id:2,title:"Batch Flower Essences",uri:"batch_flower_essences"
            },
            {
                id:3,title:"Neurotransmitters",uri:"neurotransmitters"
            },
            {
                id:4,title:"Brain Anatomy",uri:"brain_anatomy"
            },
            {
                id:5,title:"Past Tramaus Affecting",uri:"past_tramaus"
            },
        ]; 
        $scope.IsVisible = false;
        $scope.changeIcon = true;
        $scope.IsVisibleMRx = false;
        $scope.intervalRef = $interval(function () {
            for (var i = 0; i < $scope.mindParamList.length; i++) {
                $rootScope.randomMindValue[i] = parseInt(Math.random() * 999);
            }
        }, 100);
        var myTimer = $timeout(function () {
            $interval.cancel($scope.intervalRef);
            for (var i = 0; i < $rootScope.randomMindValue.length; i++) {
                if ($rootScope.randomMindValue[i] > $scope.topVal[0]) {
                    $scope.topVal[0] = $rootScope.randomMindValue[i];
                    $scope.maxIndex[0] = i;
                }
            }

            for (i = 0; i < $rootScope.randomMindValue.length; i++) {
                if ($rootScope.randomMindValue[i] > $scope.topVal[1] && $rootScope.randomMindValue[i] < $scope.topVal[0]) {
                    $scope.topVal[1] = $rootScope.randomMindValue[i];
                    $scope.maxIndex[1] = i;
                }
            }

            for (i = 0; i < $rootScope.randomMindValue.length; i++) {
                if ($rootScope.randomMindValue[i] > $scope.topVal[2] && $rootScope.randomMindValue[i] < $scope.topVal[1]) {
                    $scope.topVal[2] = $rootScope.randomMindValue[i];
                    $scope.maxIndex[2] = i;
                }
            }
            document.getElementById("mindParam" + $scope.maxIndex[0]).style.background = "pink";
            document.getElementById("mindParam" + $scope.maxIndex[1]).style.background = "pink";
            document.getElementById("mindParam" + $scope.maxIndex[2]).style.background = "pink";
        }, 1000);

        $scope.showPlayerBRx = function ()
        {
            $scope.IsVisibleBRx = true;
            $scope.changeIconBRx = false;
        };

        $scope.hidePlayerBRx = function ()
        {
            $scope.IsVisibleBRx = false;
            $scope.changeIconBRx = true;
        };
        $scope.onDropCompleteBRx = function (data, evt) {
            $scope.playItemsBRx.push(data);
        };
        $scope.showPlayerMRx = function ()
        {
            $scope.IsVisibleMRx = true;
            $scope.changeIconMRx = false;
        };
        $scope.hidePlayerMRx = function ()
        {
            $scope.IsVisibleMRx = false;
            $scope.changeIconMRx = true;
        };
    $scope.onDropCompleteMRx=function(data,evt){
        $scope.playItemsMRx.push(data);
    };
   });
});
    