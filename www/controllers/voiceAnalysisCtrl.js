angular.module('aura-genie.controllers').controller('voiceAnalysisCtrl', function($scope, $location, $interval, $rootScope, $cordovaMedia, $cordovaCapture, $stateParams, $state, $localStorage, $cordovaFileTransfer, $ionicLoading, $ionicPopup,$http,$timeout) {
    $scope.recordedAudioFile = '';
    $scope.recordingDuration = 25;
    $scope.recordingTimerPromise = '';
    $scope.time = 0;
    $scope.play=true;
    $scope.media='';
    $scope.popup='';
    $scope.hide=true;
    $scope.recDisabled=false;
    $scope.timer = function() {
        $scope.time++;
        if ($scope.time == $scope.recordingDuration) {
            $scope.recDisabled= false;
            $interval.cancel($scope.recordingTimerPromise);
        }
    };
    $scope.startRecord = function() {
        $rootScope.recordedAudioFile= undefined;
        $scope.audioTemp=undefined;
        $scope.recDisabled= true;
        $scope.hide=true;
        var options = {limit: 1, duration: $scope.recordingDuration};
        try {
            $cordovaCapture.captureAudio(options).then(function(audioData) {
                // Success! Audio data is here
                console.log("Recording Start Success");
                var i, path, len;
                for (i = 0, len = audioData.length; i < len; i += 1) {
                    $rootScope.recordedAudioFile = audioData[i].fullPath;
                }
                $scope.uploadVoice();
                console.log(audioData);
            }, function(err) {
                // An error occurred. Show a message to the user
                console.log("Recording Start Error");
            });
        } catch (e) {
            console.log("Recorder will work on Devices only");
        }
        $scope.time = 0;
        $interval.cancel($scope.recordingTimerPromise);
        $scope.recordingTimerPromise = $interval($scope.timer, 1000);
        $ionicLoading.hide();
        $scope.isRecord=true;
    };
    $scope.stopRecord = function() {
        try {
            $cordovaMedia.stop();
        } catch (e) {
            console.log("Not Stopping Record on ");
        }
        $scope.time = 0;
        $interval.cancel($scope.recordingTimerPromise);
    };
    $scope.playRecord = function() {
        console.log("Play Audio");
        
        console.log($rootScope.recordedAudioFile);
         if($scope.audioTemp== undefined)
        {
            var alertPopup = $ionicPopup.alert({
                            title: 'Error',
                            template: 'Record your voice'
                        });
        }
       else {
        try {
            $scope.recDisabled=true;
            $scope.play=false;
            console.log("play method");
            var media = $cordovaMedia.newMedia($rootScope.recordedAudioFile);
            media.play();
        } catch (e) {
            console.log("Play Sound on Devices");
        }
        $scope.time = 0;
        $interval.cancel($scope.recordingTimerPromise);
        $scope.recordingTimerPromise = $interval($scope.timer, 1000);
    }
    };
    $scope.pause=function(){
        try {
             $scope.play=true;
             $scope.recDisabled=false;
             console.log("stop method");
             var media = $cordovaMedia.newMedia($rootScope.recordedAudioFile);
             media.stop();
        } catch(e){
            console.log("exception occured"+e);
        }
        $interval.cancel($scope.recordingTimerPromise);
    };
    $scope.uploadVoice = function() {
        var server = $rootScope.apiUrl + "API/User/upload_record_file";
        //var filePath = cordova.file.documentsDirectory + "testImage.png";
        var filePath = $rootScope.recordedAudioFile;
        var trustHosts = true;
        var options = {
            params: {
                record_id: $stateParams.user_record_id,
                type: 'audio'
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email,
            },
            mimeType: 'audio/wav',
            fileName:'audio.wav',
        };
        try {
            $ionicLoading.show();
            $cordovaFileTransfer.upload(server, filePath, options)
                    .then(function(result) {
                        result = angular.fromJson(result);
                        response = angular.fromJson(result.response);

                        console.log('file upload success');
                        console.log(response);
                        
                        if (response.status) {
                            $scope.audioTemp = response.data.filename;
                            $ionicLoading.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Success',
                                template: 'Audio Uploaded Temporary.'
                            }).then(function(res) {
                                 $scope.hide=false;
                            });
                        } else {
                            $ionicLoading.hide();
                            $ionicPopup.alert({
                                title: 'Upload Audio Failed Temporary',
                                template: response.message
                            });
                        }
                        // Success!
                    }, function(err) {
                        $ionicLoading.hide();
                        console.log('Audio upload error');
                        console.log(err);
                        var alertPopup = $ionicPopup.alert({
                            title: 'API Error',
                            template: 'API is not working'
                        });
                        // Error
                    }, function(progress) {
                        $ionicLoading.hide();
                        console.log(progress);
                        // constant progress updates
                    });

        } catch (e) {
            console.log('File Transfer Plugin will work on device');
            console.log(e);
        }
    };

    $scope.remoteVoiceUpload = function() {
        if($scope.audioTemp== undefined)
        {
            var alertPopup = $ionicPopup.alert({
                            title: 'Error',
                            template: 'Record your voice'
                        });
        }
        else{

        try{           
        console.log($scope.audioTemp);
        $ionicLoading.show();
        $http({
            url: $rootScope.apiUrl + "API/User/update_user_record",
            method: 'POST',
            data: {
                record_id: $stateParams.user_record_id,
                //   record_id: 22,
                audio: $scope.audioTemp
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            $ionicLoading.hide();
            console.log('Record Updated');
            console.log(response);
            if (response.status) {
                $ionicPopup.alert({
                    title: "Success",
                    template: "Remote Voice Completed"
                });
            } else {
                $ionicPopup.alert({
                    title: "Error",
                    template: response.message
                });
            }
        }).error(function(erorr) {
            console.log('Record Updated');
            console.log(erorr);
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'failed',
                template: 'API is not working'
            });
        });
        }catch(e){
            console.log('File Transfer Plugin will work on device');
            consoel.log(e);
        }
                    
        }
    };
    
    $scope.startAnalysis = function(){
        if($scope.audioTemp== undefined)
        {
            var alertPopup = $ionicPopup.alert({
                            title: 'Error',
                            template: 'Record your voice'
                        });
        }
        else{
//        
            $scope.popup = $ionicPopup.show({
            template:'<div class="text-center" style="width:60%;margin:0 auto"><ion-spinner class="bubbles"></ion-spinner><p>Scanning voice.....</p></div>',
            delay:'10000'
        });
        $timeout(function() {
            $scope.popup.close();
            $ionicPopup.show({
                template:'<p class="text-center">Energy Scan Complete</p>',
                buttons:[{
                        text:'Continue',
                        onTap:function(){
                          $state.go("app.image_analysis",{user_record_id:$stateParams.user_record_id});  
                        }
                },
                {
                    text:'Repeat',
                    onTap:function(){
                        $scope.time = 0;
                        $state.go("app.voice_analysis",{user_record_id:$stateParams.user_record_id});
                    }
                }]
            });
        }, 3000);

        }  
    };
});    
