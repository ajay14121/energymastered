angular.module('aura-genie.controllers').controller('accountActivationCtrl', function($scope, $http, $location, $ionicPopup, $rootScope, $ionicLoading, $localStorage) {
    $scope.data = {};
    $scope.data.email = '';
    $scope.data.activation_key = '';
    $scope.status = $localStorage.user.status;
    $scope.accountActivation = function() {
        if ($scope.data.email == '') {
            $ionicPopup.alert({
                title: 'empty field',
                template: 'email is mandatory'
            });
        } else if ($scope.data.activation_key == '') {
            $ionicPopup.alert({
                title: 'empty field',
                template: 'Activation code is mandatory'
            });
        } else {
            $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/User/account_activation",
                method: 'POST',
                data: {
                    email: $scope.data.email,
                    activation_key: $scope.data.activation_key,
                },
                headers: {
                    'Access-Token': $localStorage.user.token,
                    'Email': $localStorage.user.email
                }
            }).success(function(response) {
                $ionicLoading.hide();
                if (response.status) {
                    $ionicPopup.alert({
                        title: "Activation Completed",
                        template: response.message
                    }).then(function(res) {
                        $location.path("/app/user_record_one");
                    });
                } else {
                    $ionicPopup.alert({
                        title: "Activation failed",
                        template: response.message
                    });
                }
            }).error(function(erorr) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Activation Process failed',
                    template: 'API is not working'
                });
            });
        }
    };
});
    