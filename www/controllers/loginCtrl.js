angular.module('aura-genie.controllers').controller('loginCtrl', function($scope, $http, $location, $state, $ionicPopup, $rootScope, $ionicLoading, $localStorage, $cordovaFacebook) {
    $scope.data = {};

    $scope.facebookLogin = function() {
        try {
            $cordovaFacebook.login(["public_profile", "email"])
                    .then(function(success) {
                        console.log("Facebook Login Success");
                        console.log(success.authResponse.userID);
                        $cordovaFacebook.api("me?fields=id,name,email,gender,first_name,last_name", ["public_profile"])
                                .then(function(success) {
                                    console.log("Facebook Data Access API success");
                                    console.log(success);
                                    $ionicLoading.show();
                                    $http({
                                        url: $rootScope.apiUrl + "API/User/facebook_login",
                                        method: 'post',
                                        data: {
                                            first_name: success.first_name,
                                            last_name: success.last_name,
                                            email: success.email,
                                            facebook_id: success.id,
                                            uuid: $rootScope.uuid,
                                            platform: $rootScope.platform
                                        }
                                    }).success(function(response) {
                                        $ionicLoading.hide();
                                        if (response.status) {
                                            $localStorage.user = response.data;
                                            $rootScope.user=$localStorage.user;
                                           if (response.data.status == 1) {
                                                $state.go("app.account_activation",null,{reload:true});
                                            } else if (response.data.status == 2) {
                                                $state.go("app.user_record_one",null,{reload:true});
                                            }
                                        } else {
                                            $ionicPopup.alert({
                                                title: 'Facebook Login Failed',
                                                template: response.message
                                            });
                                        }
                                    }).error(function(erorr) {
                                        $ionicLoading.hide();
                                        $ionicPopup.alert({
                                            title: 'API Error',
                                            template: 'API is not working'
                                        });
                                    });


                                }, function(error) {
                                    // error
                                    console.log("Facebook Data Access API fail");
                                });
                    }, function(error) {
                        console.log("Facebook Login Fail");
                        console.log(error);
                    });
        } catch (e) {
            console.log("Facebook Plugin is not working");
        }
    };
    $scope.login = function() {
        if ($scope.data.email == '' || $scope.data.password == '') {
            $ionicPopup.alert({
                title: 'empty field',
                template: 'email and password are mandatory'
            });
        } else if (!$rootScope.email_filter.test($scope.data.email)) {
            $ionicPopup.alert({
                title: 'invalid email',
                template: 'Please enter valid email'
            });
        } else {
            console.log($scope.data.email);
            $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/User/login",
                method: 'post',
                data: {
                    email: $scope.data.email,
                    password: $scope.data.password,
                    uuid: $rootScope.uuid,
                    platform: $rootScope.platform
                }
            }).success(function(response) {
                $ionicLoading.hide();
                if (response.status) {
                    $localStorage.user = response.data;
                    $rootScope.user=$localStorage.user;
                    if (response.data.status == 0) {
                        $state.go("otp");
                    } else if (response.data.status == 1) {
                        $state.go("app.account_activation",null,{reload:true});
                    } else {
                        $state.go("app.user_record_one",null,{reload:true});
                    }
                } else {
                    $ionicPopup.alert({
                        title: 'login failed',
                        template: response.message
                    });
                }
            }).error(function(error) {
                $ionicLoading.hide();
                console.log(error);
                $ionicPopup.alert({
                    title: 'API Error',
                    template: 'API is not working'
                });
            });

        }
    };
});
    