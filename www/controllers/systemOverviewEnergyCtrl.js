angular.module('aura-genie.controllers').controller('systemOverviewEnergyCtrl', function($scope, $interval, $rootScope, $timeout) {
    angular.element(document).ready(function () {
           
        $scope.topVal=[0,0,0];
        $scope.maxIndex=[0,0,0];
        $scope.playItemsBRx=[];
        $scope.playItemsMRx=[];
        $scope.energyParamList=[
            {
                id:0,title:"Soifeggio Tones"
            },
            {
                id:1,title:"Sacred Geometry"
            },
            {
                id:2,title:"Chakras"
            },
            {
                id:3,title:"Auras"
            },
            {
                id:4,title:"Nogier"
            }
        ]; 
        $scope.IsVisible = false;
        $scope.changeIcon = true;
        $scope.IsVisibleMRx = false;
        $scope.intervalRef = $interval(function () {
            for (var i = 0; i < $scope.energyParamList.length; i++) {
                $rootScope.randomEnergyValue[i] = parseInt(Math.random() * 999);
            }
        }, 100);
        var myTimer = $timeout(function () {
            var canceled = $interval.cancel($scope.intervalRef);
//            console.log(canceled);
            for (var i = 0; i < $rootScope.randomEnergyValue.length; i++) {
                if ($rootScope.randomEnergyValue[i] > $scope.topVal[0]) {
                    $scope.topVal[0] = $rootScope.randomEnergyValue[i];
                    $scope.maxIndex[0] = i;
                    
                }
            }
             
            for (i = 0; i < $rootScope.randomEnergyValue.length; i++) {
                if ($rootScope.randomEnergyValue[i] > $scope.topVal[1] && $rootScope.randomEnergyValue[i] < $scope.topVal[0]) {
                    $scope.topVal[1] = $rootScope.randomEnergyValue[i];
                    $scope.maxIndex[1] = i;
                }
            }

            for (i = 0; i < $rootScope.randomEnergyValue.length; i++) {
                if ($rootScope.randomEnergyValue[i] > $scope.topVal[2] && $rootScope.randomEnergyValue[i] < $scope.topVal[1]) {
                    $scope.topVal[2] = $rootScope.randomEnergyValue[i];
                    $scope.maxIndex[2] = i;
                }
            }
//            console.log("bodyParam"+$scope.maxIndex[0]);
//            console.log("bodyParam"+$scope.maxIndex[1]);
//            console.log("bodyParam"+$scope.maxIndex[2]);
            document.getElementById("energyParam" + $scope.maxIndex[0]).style.background = "pink";
            document.getElementById("energyParam" + $scope.maxIndex[1]).style.background = "pink";
            document.getElementById("energyParam" + $scope.maxIndex[2]).style.background = "pink";
            
        },1000);

        $scope.showPlayerBRx = function ()
        {
            $scope.IsVisibleBRx = true;
            $scope.changeIconBRx = false;
        };

        $scope.hidePlayerBRx = function ()
        {
            $scope.IsVisibleBRx = false;
            $scope.changeIconBRx = true;
        };
        $scope.onDropCompleteBRx = function (data, evt) {
           $scope.playItemsBRx.push(data);
        };
        $scope.showPlayerMRx = function ()
        {
            $scope.IsVisibleMRx = true;
            $scope.changeIconMRx = false;
        };
        $scope.hidePlayerMRx = function ()
        {
            $scope.IsVisibleMRx = false;
            $scope.changeIconMRx = true;
        };
       $scope.onDropCompleteMRx=function(data,evt){
//           for(var i=0;i<$scope.playItemsMRx.length;i++)
//           {
//                if (data.title == $scope.playItemsMRx[i])
//                {
//                    $ionicPopup.alert({
//                        title: 'duplicate item',
//                        template: 'item already added to player'
//                    });
//                } else
//                {
//                    $scope.playItemsMRx.push(data);
//                }
//           }
        $scope.playItemsMRx.push(data);
    };
   });
});
    