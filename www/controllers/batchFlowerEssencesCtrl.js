angular.module('aura-genie.controllers').controller('batchFlowerEssencesCtrl', function($scope, $interval, $rootScope, $timeout) {
    angular.element(document).ready(function () {
           
        $scope.topVal=[0,0,0];
        $scope.maxIndex=[0,0,0];
        $scope.playItemsBRx=[];
        $scope.playItemsMRx=[];
        																				
        $scope.batchFlowerParamList=[
            {
                id:0,title:"Agrimony"
            },
            {
                id:1,title:"Aspen"
            },
            {
                id:2,title:"Beech"
            },
            {
                id:3,title:"Centaury"
            },
            {
                id:4,title:"Cerato"
            },
            {
                id:5,title:"Chicory"
            },
            {
                id:6,title:"Clematis"
            },
            {
                id:7,title:"Crab Apple"
            },
            {
                id:8,title:"Elm"
            },
            {
                id:9,title:"Gentian"
            },
            {
                id:10,title:"Gorse"
            },
            {
                id:11,title:"Heather"
            },
            {
                id:12,title:"ch flowers"
            },
            {
                id:13,title:"Holly"
            },
            {
                id:14,title:"Hunnysuckle"
            },
            {
                id:16,title:"Homebeam"
            },
            {
                id:17,title:"Impatiens"
            },
            {
                id:18,title:"Larch"
            },
            {
                id:19,title:"Mimulus"
            },
            {
                id:20,title:"Mustard"
            },
            {
                id:21,title:"Oak"
            },
            {
                id:22,title:"Olive"
            },
            {
                id:23,title:"Pine"
            },
            {
                id:24,title:"Red Chestnut"
            },
            {
                id:25,title:"Rock Rose"
            },
            {
                id:26,title:"Rock Water"
            },
            {
                id:27,title:"Scleranthus"
            },
            {
                id:28,title:"Star of Bethlehem"
            },
            {
                id:29,title:"Sweet Chestnut"
            },
            {
                id:30,title:"Vervian"
            },
            {
                id:31,title:"Vine"
            },
            {
                id:32,title:"Walnet"
            },
            {
                id:33,title:"Water Violet"
            },
            {
                id:34,title:"White Chestnut"
            },
            {
                id:35,title:"Wild Oat"
            },
            {
                id:35,title:"Wild Rose"
            },
            {
                id:35,title:"Willlow"
            }
        ]; 
        $scope.IsVisible = false;
        $scope.changeIcon = true;
        $scope.IsVisibleMRx = false;
        $scope.intervalRef = $interval(function () {
            for (var i = 0; i < $scope.batchFlowerParamList.length; i++) {
                $rootScope.randomBatchFlowerValue[i] = parseInt(Math.random() * 999);
            }
        }, 100);
        var myTimer = $timeout(function () {
            $interval.cancel($scope.intervalRef);
            for (var i = 0; i < $rootScope.randomBatchFlowerValue.length; i++) {
                if ($rootScope.randomBatchFlowerValue[i] > $scope.topVal[0]) {
                    $scope.topVal[0] = $rootScope.randomBatchFlowerValue[i];
                    $scope.maxIndex[0] = i;
                }
            }

            for (i = 0; i < $rootScope.randomBatchFlowerValue.length; i++) {
                if ($rootScope.randomBatchFlowerValue[i] > $scope.topVal[1] && $rootScope.randomBatchFlowerValue[i] < $scope.topVal[0]) {
                    $scope.topVal[1] = $rootScope.randomBatchFlowerValue[i];
                    $scope.maxIndex[1] = i;
                }
            }

            for (i = 0; i < $rootScope.randomBatchFlowerValue.length; i++) {
                if ($rootScope.randomBatchFlowerValue[i] > $scope.topVal[2] && $rootScope.randomBatchFlowerValue[i] < $scope.topVal[1]) {
                    $scope.topVal[2] = $rootScope.randomBatchFlowerValue[i];
                    $scope.maxIndex[2] = i;
                }
            }
            document.getElementById("batchFlowerParam" + $scope.maxIndex[0]).style.background = "pink";
            document.getElementById("batchFlowerParam" + $scope.maxIndex[1]).style.background = "pink";
            document.getElementById("batchFlowerParam" + $scope.maxIndex[2]).style.background = "pink";
        }, 1000);

        $scope.showPlayerBRx = function ()
        {
            $scope.IsVisibleBRx = true;
            $scope.changeIconBRx = false;
        };

        $scope.hidePlayerBRx = function ()
        {
            $scope.IsVisibleBRx = false;
            $scope.changeIconBRx = true;
        };
        $scope.onDropCompleteBRx = function (data, evt) {
            $scope.playItemsBRx.push(data);
        };
        $scope.showPlayerMRx = function ()
        {
            $scope.IsVisibleMRx = true;
            $scope.changeIconMRx = false;
        };
        $scope.hidePlayerMRx = function ()
        {
            $scope.IsVisibleMRx = false;
            $scope.changeIconMRx = true;
        };
    $scope.onDropCompleteMRx=function(data,evt){
        $scope.playItemsMRx.push(data);
    };
   });
});
    