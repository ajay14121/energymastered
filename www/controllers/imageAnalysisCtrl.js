angular.module('aura-genie.controllers').controller('imageAnalysisCtrl', function($scope,$state, $http, $stateParams, $location, $rootScope, $cordovaCamera, $cordovaFileTransfer, $ionicLoading, $ionicPopup, $localStorage) {
    $scope.captureImage = function() {

        try {
            var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };
            $cordovaCamera.getPicture(options).then(function(imageData) {
                console.log('Camera Capture Success');
                console.log(imageData);
                $scope.imgURI = "data:image/jpeg;base64," + imageData;
                $scope.uploadImage();
            }, function(err) {
                // An error occured. Show a message to the user
                console.log('Camera Capture Error');
                console.log(err);
            });
        } catch (e) {
            console.log('Camera Plugin only work on device');
        }

    };

    $scope.getImage = function() {
        try {
            var options = {
                quality: 50,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: true,
                targetWidth: 200,
                targetHeight: 200

            };

            $cordovaCamera.getPicture(options).then(function(imageURI) {
                console.log('Camera getPicture Success');
                console.log(imageURI);
                $scope.imgURI = imageURI;
                $scope.uploadImage();
            }, function(err) {
                // error
                console.log('Camera getPicture Error');
                console.log(err);
            });
        } catch (e) {
            console.log('Camera Plugin only work on device');
        }
    };

    $scope.uploadImage = function() {
        var server = $rootScope.apiUrl + "API/User/upload_record_file";
        //var filePath = cordova.file.documentsDirectory + "testImage.png";
        var filePath = $scope.imgURI;
        var trustHosts = true;
        var options = {
            params: {
                record_id: $stateParams.user_record_id,
                type: 'image'
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email,
            }
        };
        try {
            $ionicLoading.show();
            $cordovaFileTransfer.upload(server, filePath, options)
                    .then(function(result) {
                        result = angular.fromJson(result);
                        response = angular.fromJson(result.response);

                        console.log('file upload success');
                        console.log(response);
                        $ionicLoading.hide();
                        if (response.status) {
                            $scope.imgURITemp = response.data.filename;
                            var alertPopup = $ionicPopup.alert({
                                title: 'Success',
                                template: 'Image Uploaded Temporary.'
                            });
                        } else {
                            $ionicPopup.alert({
                                title: 'Upload Image Failed Temporary',
                                template: response.message
                            });
                        }
                        // Success!
                    }, function(err) {
                        $ionicLoading.hide();
                        console.log('file upload error');
                        console.log(err);
                        var alertPopup = $ionicPopup.alert({
                            title: 'API Error',
                            template: 'API is not working'
                        });
                        // Error
                    }, function(progress) {
                        $ionicLoading.hide();
                        console.log(progress);
                        // constant progress updates
                    });

        } catch (e) {
            console.log('File Tarfer Plugin will work on device');
            consoel.log(e);
        }
    };

    $scope.beginScan = function() {
        console.log($scope.imgURITemp);
        if($scope.imgURITemp== undefined)
        {
            var alertPopup = $ionicPopup.alert({
                            title: 'Error',
                            template: 'Upload your Image'
                        });
        }
        else{
        //$ionicLoading.show();
        $scope.popup = $ionicPopup.show({
            template:'<div class="text-center" style="width:60%;margin:0 auto"><ion-spinner class="bubbles"></ion-spinner><p>Scanning voice.....</p></div>',
            delay:'10000'
        });
        $http({
            url: $rootScope.apiUrl + "API/User/update_user_record",
            method: 'POST',
            data: {
                record_id: $stateParams.user_record_id,
                //   record_id: 22,
                image: $scope.imgURITemp
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {            
            //$ionicLoading.hide();
            console.log('Record Updated');
            console.log(response);
            if (response.status) {
                 $scope.popup.close();
                 $ionicPopup.show({
                template:'<p class="text-center">Energy Scan Complete</p>',
                buttons:[{
                        text:'Continue',
                        onTap:function(){
                          $state.go("app.system_overview",{user_record_id:$stateParams.user_record_id});  
                        }
                },
                {
                    text:'Repeat',
                    onTap:function(){
                        $scope.time = 0;
                        $state.go("app.image_analysis",{user_record_id:$stateParams.user_record_id});
                    }
                }]
            });
                
//                $ionicPopup.alert({
//                    title: "Success",
//                    template: "Scan Completed"
//                
//            });
//                     .then(function(res) {
//                    $location.path("/app/system_overview");
//                });
            } else {
                $ionicPopup.alert({
                    title: "Error",
                    template: response.message
                });
            }
        }).error(function(erorr) {
            console.log('Record Updated');
            console.log(erorr);
            $scope.popup.close();
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'failed',
                template: 'API is not working'
            });
        });
    }
    };
    
});
    