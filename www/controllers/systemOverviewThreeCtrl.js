angular.module('aura-genie.controllers').controller('systemOverviewThreeCtrl', function($scope, $interval, $rootScope, $timeout, $cordovaMedia, $cordovaCapture) {
        
        $scope.topVal=[0,0,0];
        $scope.maxIndex=[0,0,0];
        $scope.playItemsBRx=[];
        $scope.playItemsMRx=[]; 
        $scope.itemCountBRx=0;
        $scope.itemCountMRx=0;
        $scope.IsVisible = false;
        $scope.changeIcon = true;
        $scope.IsVisibleMRx = false;
        $scope.random=[];
        var temp=$rootScope.system_overview[$rootScope.index].uri;
        $scope.paramGroup=$rootScope.system_overview[temp][$rootScope.paramList];
        $scope.title=$rootScope.system_overview[temp][$rootScope.indexSub].title;
        for(var i=0;i<$scope.paramGroup.length;i++)
        {
            $scope.random[i]=$scope.paramGroup[i].randomValue;
        }
        
            for (var i = 0; i < $scope.paramGroup.length; i++) {
                if ($scope.paramGroup[i].randomValue > $scope.topVal[0]) {
                    $scope.topVal[0] = $scope.paramGroup[i].randomValue;
                    $scope.maxIndex[0] = i;
                } 
            }
             
            for (i = 0; i < $scope.paramGroup.length; i++) {
                if ($scope.paramGroup[i].randomValue > $scope.topVal[1] && $scope.paramGroup[i].randomValue < $scope.topVal[0]) {
                    $scope.topVal[1] = $scope.paramGroup[i].randomValue;
                    $scope.maxIndex[1] = i;
                }
            }
            for (i = 0; i < $scope.paramGroup.length; i++) {
                if ($scope.paramGroup[i].randomValue > $scope.topVal[2] && $scope.paramGroup[i].randomValue < $scope.topVal[1]) {
                    $scope.topVal[2] = $scope.paramGroup[i].randomValue;
                    $scope.maxIndex[2] = i;
                }
            }
            for(var i=0;i<$scope.maxIndex.length;i++)
            {
                var indexTop=$scope.maxIndex[i];
                $scope.paramGroup[indexTop].topClassActive = 'active';
            }
      

        $scope.showPlayerBRx = function ()
        {
            $scope.IsVisibleBRx = true;
            $scope.changeIconBRx = false;
        };

        $scope.hidePlayerBRx = function ()
        {
            $scope.IsVisibleBRx = false;
            $scope.changeIconBRx = true;
        };
        $scope.onDropCompleteBRx = function (data, evt) {
            $scope.matches = true;
            angular.forEach($scope.playItemsBRx, function(item) {
                if (data.id === item.id) {
                    $scope.matches = false;
                }
            });

            // add item to collection
            if ($scope.matches != false) {
                 $scope.playItemsBRx.push(data);
            }
        
        $scope.itemCountBRx=$scope.playItemsBRx.length;
        };
        $scope.showPlayerMRx = function ()
        {
            $scope.IsVisibleMRx = true;
            $scope.changeIconMRx = false;
        };
        $scope.hidePlayerMRx = function ()
        {
            $scope.IsVisibleMRx = false;
            $scope.changeIconMRx = true;
        };
    $scope.onDropCompleteMRx=function(data,evt){
        $scope.matches = true;
            angular.forEach($scope.playItemsMRx, function(item) {
                if (data.id === item.id) {
                    $scope.matches = false;
                }
            });

            // add item to collection
            if ($scope.matches != false) {
                 $scope.playItemsMRx.push(data);
            }
        
        $scope.itemCountMRx=$scope.playItemsMRx.length;
    };
});
    