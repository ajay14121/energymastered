angular.module('aura-genie.controllers').controller('neuroCtrl', function($scope, $interval, $rootScope, $timeout, $cordovaMedia, $cordovaCapture) {
    angular.element(document).ready(function () {
           
        $scope.topVal=[0,0,0];
        $scope.maxIndex=[0,0,0];
        $scope.playItemsBRx=[];
        $scope.playItemsMRx=[];
        $scope.neuroParamList=[
            {
                id:0,title:"Serotonin"
            },
            {
                id:1,title:"Gaba"
            },
            {
                id:2,title:"Dopamine"
            },
            {
                id:3,title:"Norepinephr"
            },
            {
                id:4,title:"Epinephrine"
            }
        ]; 
        $scope.IsVisible = false;
        $scope.changeIcon = true;
        $scope.IsVisibleMRx = false;
        $scope.intervalRef = $interval(function () {
            for (var i = 0; i < $scope.neuroParamList.length; i++) {
                $rootScope.randomNeuroValue[i] = parseInt(Math.random() * 999);
            }
        }, 100);
        var myTimer = $timeout(function () {
            $interval.cancel($scope.intervalRef);
            for (var i = 0; i < $rootScope.randomNeuroValue.length; i++) {
                if ($rootScope.randomNeuroValue[i] > $scope.topVal[0]) {
                    $scope.topVal[0] = $rootScope.randomNeuroValue[i];
                    $scope.maxIndex[0] = i;
                }
            }

            for (i = 0; i < $rootScope.randomNeuroValue.length; i++) {
                if ($rootScope.randomNeuroValue[i] > $scope.topVal[1] && $rootScope.randomNeuroValue[i] < $scope.topVal[0]) {
                    $scope.topVal[1] = $rootScope.randomNeuroValue[i];
                    $scope.maxIndex[1] = i;
                }
            }

            for (i = 0; i < $rootScope.randomNeuroValue.length; i++) {
                if ($rootScope.randomNeuroValue[i] > $scope.topVal[2] && $rootScope.randomNeuroValue[i] < $scope.topVal[1]) {
                    $scope.topVal[2] = $rootScope.randomNeuroValue[i];
                    $scope.maxIndex[2] = i;
                }
            }
            document.getElementById("neuroParam" + $scope.maxIndex[0]).style.background = "pink";
            document.getElementById("neuroParam" + $scope.maxIndex[1]).style.background = "pink";
            document.getElementById("neuroParam" + $scope.maxIndex[2]).style.background = "pink";
        }, 1000);

        $scope.showPlayerBRx = function ()
        {
            $scope.IsVisibleBRx = true;
            $scope.changeIconBRx = false;
        };

        $scope.hidePlayerBRx = function ()
        {
            $scope.IsVisibleBRx = false;
            $scope.changeIconBRx = true;
        };
        $scope.onDropCompleteBRx = function (data, evt) {
            $scope.playItemsBRx.push(data);
        };
        $scope.showPlayerMRx = function ()
        {
            $scope.IsVisibleMRx = true;
            $scope.changeIconMRx = false;
        };
        $scope.hidePlayerMRx = function ()
        {
            $scope.IsVisibleMRx = false;
            $scope.changeIconMRx = true;
        };
    $scope.onDropCompleteMRx=function(data,evt){
        $scope.playItemsMRx.push(data);
    };
   });
});
    