angular.module('aura-genie.controllers').controller('systemOverviewOrgansCtrl', function($scope, $interval, $rootScope, $timeout, $cordovaMedia, $cordovaCapture) {
    angular.element(document).ready(function () {
         				 
        $scope.topVal=[0,0,0];
        $scope.maxIndex=[0,0,0];
        $scope.playItemsBRx=[];
        $scope.playItemsMRx=[];
        $scope.organsParamList=[
            {
                id:0,title:"Brain",uri:"brain"
            },
            {
                id:1,title:"Eyes",uri:"eyes"
            },
            {
                id:2,title:"Skin",uri:"skin"
            },
            {
                id:3,title:"Thyroid",uri:"thyroid"
            },
            {
                id:4,title:"Lungs",uri:"lungs"
            },
            {
                id:5,title:"Heart",uri:"heart"
            },
            {
                id:6,title:"Liver",uri:"liver"
            },
            {
                id:7,title:"Pancreas",uri:"pancreas"
            },
            {
                id:8,title:"Stomach",uri:"stomach"
            },
            {
                id:9,title:"Spleen",uri:"spleen"
            },
            {
                id:10,title:"Kidneys",uri:"kidneys"
            },
            {
                id:11,title:"Prostate",uri:"prostate"
            },
            {
                id:12,title:"Bladder",uri:"bladder"
            },
         
        ]; 
        $scope.IsVisible = false;
        $scope.changeIcon = true;
        $scope.IsVisibleMRx = false;
        $scope.intervalRef = $interval(function () {
            for (var i = 0; i < $scope.organsParamList.length; i++) {
                $rootScope.randomOrgansValue[i] = parseInt(Math.random() * 999);
            }
        }, 100);
        var myTimer = $timeout(function () {
            $interval.cancel($scope.intervalRef);
            for (var i = 0; i < $rootScope.randomOrgansValue.length; i++) {
                if ($rootScope.randomOrgansValue[i] > $scope.topVal[0]) {
                    $scope.topVal[0] = $rootScope.randomOrgansValue[i];
                    $scope.maxIndex[0] = i;
                }
            }

            for (i = 0; i < $rootScope.randomOrgansValue.length; i++) {
                if ($rootScope.randomOrgansValue[i] > $scope.topVal[1] && $rootScope.randomOrgansValue[i] < $scope.topVal[0]) {
                    $scope.topVal[1] = $rootScope.randomOrgansValue[i];
                    $scope.maxIndex[1] = i;
                }
            }

            for (i = 0; i < $rootScope.randomOrgansValue.length; i++) {
                if ($rootScope.randomOrgansValue[i] > $scope.topVal[2] && $rootScope.randomOrgansValue[i] < $scope.topVal[1]) {
                    $scope.topVal[2] = $rootScope.randomOrgansValue[i];
                    $scope.maxIndex[2] = i;
                }
            }
            document.getElementById("organsParam" + $scope.maxIndex[0]).style.background = "pink";
            document.getElementById("organsParam" + $scope.maxIndex[1]).style.background = "pink";
            document.getElementById("organsParam" + $scope.maxIndex[2]).style.background = "pink";
        }, 1000);

        $scope.showPlayerBRx = function ()
        {
            $scope.IsVisibleBRx = true;
            $scope.changeIconBRx = false;
        };

        $scope.hidePlayerBRx = function ()
        {
            $scope.IsVisibleBRx = false;
            $scope.changeIconBRx = true;
        };
        $scope.onDropCompleteBRx = function (data, evt) {
            $scope.playItemsBRx.push(data);
        };
        $scope.showPlayerMRx = function ()
        {
            $scope.IsVisibleMRx = true;
            $scope.changeIconMRx = false;
        };
        $scope.hidePlayerMRx = function ()
        {
            $scope.IsVisibleMRx = false;
            $scope.changeIconMRx = true;
        };
    $scope.onDropCompleteMRx=function(data,evt){
        $scope.playItemsMRx.push(data);
    };
   });
});
    