angular.module('aura-genie.controllers').controller('userRecordsCtrl', function($scope, $http, $location, $ionicPopup, $rootScope, $ionicLoading, $localStorage) {
    $scope.data = {};
    $scope.data.users = '';
    $ionicLoading.show();
    $http({
        url: $rootScope.apiUrl + "API/User/list_user_record",
        method: 'POST',
        headers: {
            'Access-Token': $localStorage.user.token,
            'Email': $localStorage.user.email
        }
    }).success(function(response) {
        $ionicLoading.hide();
        if (response.status) {
            $scope.data.users = response.data;
        } else {
            $ionicPopup.alert({
                title: "Error",
                template: response.message
            });
        }
    }).error(function(erorr) {
        $ionicLoading.hide();
        $ionicPopup.alert({
            title: 'API/User/list_user_record',
            template: 'API is not working'
        });
    });
});
    